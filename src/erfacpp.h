#include <erfa.h>
#include <erfaiers.h>
#include <erfatime.h>

#include <cmath>
#include <iomanip>
#include <iostream>

namespace erfacpp {

const double ARCSEC_TO_DEG = 1. / 3600;
const double ARCSEC_TO_RADIANS = M_PI / 180 * ARCSEC_TO_DEG;
double I3[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

class ERFAContext : public eraASTROM {
 public:
  ERFAContext(UTCTime time) : table(ERFAFactory::GetInstance()) {
    UpdateTime(time);
  }
  void UpdateTime(UTCTime time) {
    utc_time_ = time;
    tt_time_ = time.to<TTTime>();
    ut1_time_ = time.to<UT1Time>(*table);
    tio_locator = tt_time_.ToTIO();

    PrepareOrbitalParameters();
    ComputeCIRSITRSConversionParameters();
  }

 private:
  void PrepareOrbitalParameters() {
    double position_velocity_barycentric[2][3];
    double position_velocity_heliocentric[2][3];
    double rbpn[3][3];

    // Derive baryocentric and heliocentric position/velocities
    eraEpv00(tt_time_.djm0, tt_time_.djm, position_velocity_heliocentric,
             position_velocity_barycentric);

    double CIP_x, CIP_y;

    // Derive earth rotation axes
    eraPnm06a(tt_time_.djm0, tt_time_.djm, rbpn);
    eraBpn2xy(rbpn, &CIP_x, &CIP_y);
    const double CIO_s = eraS06(tt_time_.djm0, tt_time_.djm, CIP_x, CIP_y);

    // Precompute parameters for J2000->CIRS->ITRS conversion
    eraApci(tt_time_.djm0, tt_time_.djm, position_velocity_barycentric,
            position_velocity_heliocentric[0], CIP_x, CIP_y, CIO_s, this);
  };

  void polar_motion(UTCTime time, double& xp, double& yp) {
    // FOR NOW
    std::shared_ptr<IERSTable> table = ERFAFactory::GetInstance();
    xp = table->GetValue("PM_x", time.mjd()) * ARCSEC_TO_RADIANS;
    yp = table->GetValue("PM_y", time.mjd()) * ARCSEC_TO_RADIANS;

    if (std::isnan(xp)) xp = 1.696847883883376e-07;
    if (std::isnan(yp)) yp = 1.4059596752176543e-06;
  }
  void ComputeCIRSITRSConversionParameters() {
    polar_motion(utc_time_, polar_motion_x, polar_motion_y);

    eraPom00(polar_motion_x, polar_motion_y, tio_locator,
             polar_rotation_matrix);

    const double era = eraEra00(ut1_time_.djm0, ut1_time_.djm);

    eraC2tcio(I3, era, polar_rotation_matrix,
              celestial_terrestrial_rotation_matrix);
  }

 public:
  UTCTime utc_time_;
  TTTime tt_time_;
  UT1Time ut1_time_;
  double tio_locator;
  double polar_motion_x;
  double polar_motion_y;
  // Polar rotation matrix
  double polar_rotation_matrix[3][3];
  // Celestial to terrestial
  double celestial_terrestrial_rotation_matrix[3][3];
  std::shared_ptr<IERSTable> table;
};

typedef struct vec2d {
  double phi;
  double theta;
  friend std::ostream& operator<<(std::ostream& os, const struct vec2d& obj) {
    os << "(" << std::setprecision(9) << obj.phi << "," << obj.theta << ")";
    return os;
  }
} direction;

typedef struct vec3d {
  double x;
  double y;
  double z;

  friend std::ostream& operator<<(std::ostream& os, const struct vec3d& obj) {
    os << "(" << std::setprecision(9) << obj.x << "," << obj.y << "," << obj.z
       << ")";
    return os;
  }
} position;

void J2000_to_CIRS(ERFAContext ctx, direction j2000, UTCTime time,
                   direction& cirs) {
  direction icrs;
  eraAtccq(j2000.phi, j2000.theta, 0, 0, 0, 0, &ctx, &icrs.phi, &icrs.theta);
  eraAtciq(icrs.phi, icrs.theta, 0, 0, 0, 0, &ctx, &cirs.phi, &cirs.theta);
}
void J2000_to_CIRS(direction j2000, UTCTime time, direction& cirs) {
  ERFAContext ctx(time);
  J2000_to_CIRS(ctx, j2000, time, cirs);
}

void J2000_to_HADEC(direction j2000, UTCTime time, position observer,
                    direction& hadec) {
  eraASTROM parameters;

  double elong, phi, hm;
  double eo;
  eraGc2gd(1, &observer.x, &elong, &phi, &hm);
  double deltaut1 = 0;
  eraApco13(time.djm0, time.djm, 0, elong, phi, hm, 0, 0, 0, 0, 0, 0,
            &parameters, &eo);

  direction itrf;
  eraAtccq(j2000.phi, j2000.theta, 0, 0, 0, 0, &parameters, &hadec.phi,
           &hadec.theta);
}

void J2000_to_GCRS(direction j2000, UTCTime time, direction& gcrs) {
  double eo = 0;
  J2000_to_CIRS(j2000, time, gcrs);
  gcrs.phi = eraAnp(gcrs.phi - eo);
}

void J2000_to_ITRF(direction j2000, UTCTime time, position& itrf) {
  direction cirs;
  ERFAContext ctx(time);
  J2000_to_CIRS(ctx, j2000, time, cirs);

  const double x = std::cos(cirs.phi) * std::cos(cirs.theta);
  const double y = std::sin(cirs.phi) * std::cos(cirs.theta);
  const double z = std::sin(cirs.theta);

  const auto& c2im = ctx.celestial_terrestrial_rotation_matrix;
  itrf.x = c2im[0][0] * x + c2im[0][1] * y + c2im[0][2] * z;
  itrf.y = c2im[1][0] * x + c2im[1][1] * y + c2im[1][2] * z;
  itrf.z = c2im[2][0] * x + c2im[2][1] * y + c2im[2][2] * z;
}

}  // namespace erfacpp
