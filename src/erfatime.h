#ifndef ERFACPP_TIME
#define ERFACPP_TIME
#include <erfa.h>
#include <erfaiers.h>

#include <iostream>

namespace erfacpp {

template <class TBase>
struct OTime {
  double djm0;
  double djm;

  template <typename T>
  T to() {
    throw std::runtime_error("conversion not implemented");
  }

  template <typename T>
  T to(IERSTable&) {
    throw std::runtime_error("conversion not implemented");
  }

  const double mjd() { return djm; }

  friend std::ostream& operator<<(std::ostream& os, const OTime& obj) {
    os << "(" << obj.djm0 << ",\t" << obj.djm << ")";
    return os;
  }
};

struct UTCTime : OTime<UTCTime> {
 public:
  static UTCTime FromMJD(double mjd) {
    UTCTime res;
    res.djm0 = 2400000.5;
    res.djm = mjd;
    return res;
  }
};

struct TTTime : OTime<TTTime> {
  const double ToTIO() { return eraSp00(djm0, djm); };
};
struct TAITime : OTime<TAITime> {};
struct UT1Time : OTime<UT1Time> {};

template <>
template <>
TAITime OTime<UTCTime>::to<TAITime>() {
  TAITime res;
  eraUtctai(djm0, djm, &res.djm0, &res.djm);
  return res;
}

template <>
template <>
TTTime OTime<TAITime>::to<TTTime>() {
  TTTime res;
  eraTaitt(djm0, djm, &res.djm0, &res.djm);
  return res;
}

template <>
template <>
UT1Time OTime<UTCTime>::to<UT1Time>(IERSTable& table) {
  UT1Time result;
  double dut1 = table.GetValue("U1UTC", djm);
  if (std::isnan(dut1)) {
    dut1 = 0;
  }
  eraUtcut1(djm0, djm, dut1, &result.djm0, &result.djm);
  return result;
}

template <>
template <>
UT1Time OTime<UTCTime>::to<UT1Time>() {
  std::shared_ptr<IERSTable> table_ptr = ERFAFactory::GetInstance();
  return this->to<UT1Time>(*table_ptr);
}

template <>
template <>
TTTime OTime<UTCTime>::to<TTTime>() {
  return this->to<TAITime>().to<TTTime>();
}

}  // namespace erfacpp

#endif  // ERFACPP_TIME