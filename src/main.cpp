
#include <casacore/measures/Measures/MCDirection.h>
#include <casacore/measures/Measures/MDirection.h>
#include <casacore/measures/Measures/MEpoch.h>
#include <casacore/measures/Measures/MPosition.h>
#include <casacore/measures/Measures/MeasConvert.h>
#include <casacore/measures/Measures/MeasFrame.h>
#include <casacore/measures/Measures/SolarPos.h>
#include <erfacpp.h>

#include <iostream>
#include <string>
using casacore::MDirection;
using casacore::MeasFrame;
using casacore::MVPosition;
using casacore::SolarPos;
using erfacpp::direction;
using erfacpp::position;

const direction test_dir{1.234, 1.57};
const position test_position{826577.022720000, 461022.995082000, 5064892.814};
const double mjd = 60395.47027855601;
const casacore::MEpoch epoch(casacore::Quantity(mjd, casacore::Unit('d')));

void j2000_to_jnat_casacore(direction d, position p, direction& d_new) {
  casacore::MVDirection in = casacore::MVDirection(d.phi, d.theta);

  casacore::MVPosition mv_position(p.x, p.y, p.z);
  casacore::MPosition m_position(mv_position, casacore::MPosition::ITRF);
  MeasFrame frame = casacore::MeasFrame(epoch, m_position);

  MDirection dir(in, MDirection::J2000);
  casacore::MDirection::Convert converter = casacore::MDirection::Convert(
      dir, casacore::MDirection::Ref(casacore::MDirection::JNAT, frame));
  const casacore::MDirection jnat = converter();
  const casacore::MVDirection converted = jnat.getValue();
  d_new.phi = converted.getLong();
  d_new.theta = converted.getLat();
}

void j2000_to_jnat(direction d, position p, direction& d_new) {
  SolarPos pos(SolarPos::STANDARD);
  double epoch = casacore::MEpoch().getData()->getVector()[0];
  std::cout << "Epoch at 0 " << casacore::MEpoch() << std::endl;
  std::cout << "Epoch at 0 " << epoch << std::endl;
  MVPosition solar_pos = pos(0);
}

void erfa_j2000_to_cirs(direction d, position p, direction& d_new) {
  erfacpp::J2000_to_CIRS(d, erfacpp::UTCTime::FromMJD(mjd), d_new);
}

void erfa_j2000_to_gcrs(direction d, position p, direction& d_new) {
  erfacpp::J2000_to_GCRS(d, erfacpp::UTCTime::FromMJD(mjd), d_new);
}

void j2000_to_ITRF_casacore(direction d, position p, direction& d_new) {
  casacore::MVDirection in = casacore::MVDirection(d.phi, d.theta);

  casacore::MVPosition mv_position(p.x, p.y, p.z);
  casacore::MPosition m_position(mv_position, casacore::MPosition::ITRF);
  MeasFrame frame = casacore::MeasFrame(epoch, m_position);

  MDirection dir(in, MDirection::J2000);
  casacore::MDirection::Convert converter = casacore::MDirection::Convert(
      dir, casacore::MDirection::Ref(casacore::MDirection::ITRF, frame));
  const casacore::MDirection itrf = converter();
  const casacore::MVDirection converted = itrf.getValue();
  d_new.phi = converted.getLong();
  d_new.theta = converted.getLat();
  std::cout << converted.getVector() << std::endl;
}

void j2000_to_AZEL_casacore(direction d, position p, direction& d_new) {
  casacore::MVDirection in = casacore::MVDirection(d.phi, d.theta);

  casacore::MVPosition mv_position(p.x, p.y, p.z);
  casacore::MPosition m_position(mv_position, casacore::MPosition::ITRF);
  MeasFrame frame = casacore::MeasFrame(epoch, m_position);

  MDirection dir(in, MDirection::J2000);
  casacore::MDirection::Convert converter = casacore::MDirection::Convert(
      dir, casacore::MDirection::Ref(casacore::MDirection::AZEL, frame));
  const casacore::MDirection azel = converter();
  const casacore::MVDirection converted = azel.getValue();
  d_new.phi = converted.getLong();
  d_new.theta = converted.getLat();
}

void j2000_to_HADEC_casacore(direction d, position p, direction& d_new) {
  casacore::MVDirection in = casacore::MVDirection(d.phi, d.theta);

  casacore::MVPosition mv_position(p.x, p.y, p.z);
  casacore::MPosition m_position(mv_position, casacore::MPosition::ITRF);
  MeasFrame frame = casacore::MeasFrame(epoch, m_position);

  MDirection dir(in, MDirection::J2000);
  casacore::MDirection::Convert converter = casacore::MDirection::Convert(
      dir, casacore::MDirection::Ref(casacore::MDirection::HADEC, frame));
  const casacore::MDirection hadec = converter();
  const casacore::MVDirection converted = hadec.getValue();
  d_new.phi = converted.getLong();
  d_new.theta = converted.getLat();
}

void erfa_j2000_to_hadec(direction d, position p, direction& d_new) {
  erfacpp::J2000_to_HADEC(d, erfacpp::UTCTime::FromMJD(mjd), p, d_new);
}
void erfa_j2000_to_itrf(direction d, position& p_new) {
  erfacpp::J2000_to_ITRF(d, erfacpp::UTCTime::FromMJD(mjd), p_new);
}

int main(int argc, char* argv[]) {
  std::cout << "Welcome at fun with ERFA.\n Your coordinate transformation "
               "best library.\n"
            << std::endl;

  erfacpp::IERSTable table(
      std::string("/home/mancini/git/funwitherfa/src/tables/finals2000A.all"));

  std::cout << table.GetValue("mjd", 41703) << std::endl;

  std::cout << table.GetValue("PM_x", 41703) << std::endl;

  direction new_dir;
  direction dir_cirs{2.8503633615820227, 1.5686321729185995};
  direction dir_itrf{0.007436305256449476, 1.3858688716237009};
  position pos_itrf{-0.00213085, 0.00037689, 0.99999766};
  position new_pos;

  std::cout << "J2000_to_JNAT_casacore" << std::endl;
  j2000_to_jnat_casacore(test_dir, test_position, new_dir);
  std::cout << "Direction before: " << test_dir << std::endl;
  std::cout << "Direction after: " << new_dir << std::endl;

  std::cout << "J2000_to_CIRS_erfa" << std::endl;

  erfa_j2000_to_cirs(test_dir, test_position, new_dir);
  std::cout << "Direction before: " << test_dir << std::endl;
  std::cout << "Direction after: " << new_dir << std::endl;
  std::cout << "Direction expected: " << dir_cirs << std::endl;

  std::cout << "J2000_to_ITRF_casacore" << std::endl;
  j2000_to_ITRF_casacore(test_dir, test_position, new_dir);
  std::cout << "Direction before: " << test_dir << std::endl;
  std::cout << "Direction after: " << new_dir << std::endl;

  std::cout << "J2000_to_GCRS_erfa" << std::endl;
  erfa_j2000_to_gcrs(test_dir, test_position, new_dir);
  std::cout << "Direction before: " << test_dir << std::endl;
  std::cout << "Direction after: " << new_dir << std::endl;

  std::cout << "J2000_to_AZEL_casacore" << std::endl;
  j2000_to_AZEL_casacore(test_dir, test_position, new_dir);
  std::cout << "Direction before: " << test_dir << std::endl;
  std::cout << "Direction after: " << new_dir << std::endl;

  std::cout << "J2000_to_ITRF_erfa" << std::endl;
  erfa_j2000_to_itrf(test_dir, new_pos);
  std::cout << "Direction before: " << test_dir << std::endl;
  std::cout << "Direction after: " << new_pos << std::endl;
  std::cout << "Expected direction: " << pos_itrf << std::endl;

  std::cout << "J2000_to_HADEC_casacore" << std::endl;
  j2000_to_HADEC_casacore(test_dir, test_position, new_dir);
  std::cout << "Direction before: " << test_dir << std::endl;
  std::cout << "Direction after: " << new_dir << std::endl;

  std::cout << "J2000_to_hadec_erfa" << std::endl;
  erfa_j2000_to_hadec(test_dir, test_position, new_dir);
  std::cout << "Direction before: " << test_dir << std::endl;
  std::cout << "Direction after: " << new_dir << std::endl;
  return 0;
}