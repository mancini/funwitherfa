#ifndef ERFACPP_IERS
#define ERFACPP_IERS

#include <fstream>
#include <iostream>
#include <limits>
#include <memory>
#include <string>
#include <vector>

namespace erfacpp {

std::string derive_path_to_iers() {
  char* path = std::getenv("IERS_TABLE");
  if (path == nullptr) {
    return std::string("/var/share/iers/finals2000A.all");
  }
  return std::string(path);
}

std::string IERS_PATH = derive_path_to_iers();

class TableParser {
 public:
  TableParser(const std::string& path) : path_{path} {};

  inline const int GetColumnIndex(const std::string& name) {
    return col_names_[name];
  }

  inline const std::pair<double, double> GetLimits(const std::string& name) {
    return limits_[name];
  }
  inline const std::pair<int, int> GetIndexLimits(const std::string& name) {
    return limits_index_[name];
  }
  const std::map<std::string, int> ColumnIndexes() { return col_names_; }

  void parse() {
    std::ifstream f_stream_in = std::ifstream(path_);

    const std::vector<int> skip_map = SkipMap();
    const size_t n_columns = cols_sizes_.size();

    for (std::string line; std::getline(f_stream_in, line);) {
      const int current_line = values.size();
      std::vector<double> current_values;
      for (int col_idx = 0; col_idx < n_columns; col_idx++) {
        if (use_cols_[col_idx] && line.size()) {
          const std::string value =
              line.substr(skip_map[col_idx], cols_sizes_[col_idx]);
          try {
            current_values.push_back(std::stod(value));
          } catch (std::invalid_argument e) {
            current_values.push_back(std::numeric_limits<double>::quiet_NaN());
          }
        }
      }
      if (current_values.size()) values.push_back(current_values);
    }
    is_valid = values.size() > 0;

    if (!is_valid) {
      std::cerr << "WARNING\n"
                << "IERSTable not found. Coordinates transformation will have "
                   "lesser precision.\n"
                   "Install them in /var/share/iers/finals2000A.all\n"
                   "or define IERS_TABLE environment variable to point "
                   "to them"
                << std::endl;
    }
  };

 protected:
  std::vector<std::vector<double>> values;
  void AddColumn(const short size, const std::string& name, const bool store) {
    if (store) col_names_[name] = col_names_.size();
    cols_sizes_.push_back(size);
    use_cols_.push_back(store);
  }

  void ComputeLimits(int index_column) {
    for (const auto name_idx : col_names_) {
      const std::string variable_name = name_idx.first;
      const int column_index = name_idx.second;
      const int min_index = 0;
      int max_index = 0;
      double max_index_value = 0;
      double min_index_value = 0;
      // Table was not there skip reading
      if (!is_valid) {
        min_index_value = 0;
        max_index_value = 0;
      } else {
        min_index_value = values[0][index_column];

        for (int i = values.size() - 1; i >= 0; i--) {
          if (!std::isnan(values[i][column_index])) {
            max_index_value = values[i][index_column];
            max_index = i;
            break;
          }
        }
      }
      limits_index_[variable_name] = {0, max_index};
      limits_[variable_name] = {min_index_value, max_index_value};
    }
  }

  bool is_valid = false;

 private:
  std::string path_;
  std::vector<int> cols_sizes_;
  std::vector<bool> use_cols_;
  std::map<std::string, int> col_names_;
  std::map<std::string, std::pair<int, int>> limits_index_;
  std::map<std::string, std::pair<double, double>> limits_;

  const std::vector<int> SkipMap() {
    std::vector<int> skip_map(1);

    skip_map[0] = 0;
    for (int i = 0; i < cols_sizes_.size(); i++) {
      skip_map.push_back(cols_sizes_[i] + skip_map[i]);
    }
    return skip_map;
  };
};

class PMTable {};

class IERSTable : public TableParser {
 public:
  IERSTable(const std::string path) : TableParser(path) {
    AddColumn(2, "year", false);
    AddColumn(2, "month", false);
    AddColumn(2, "day", false);
    AddColumn(1, "space", false);
    AddColumn(8, "mdj", true);
    AddColumn(1, "space", false);
    AddColumn(1, "Flag_PM", false);
    AddColumn(1, "space", false);
    AddColumn(9, "PM_x", true);
    AddColumn(9, "PM_x_err", true);
    AddColumn(1, "space", false);
    AddColumn(9, "PM_y", true);
    AddColumn(9, "PM_y_err", true);
    AddColumn(2, "space", false);
    AddColumn(1, "Flag_U1UTC", false);
    AddColumn(10, "U1UTC", true);
    AddColumn(10, "U1UTC_err", true);

    parse();

    ComputeLimits(GetColumnIndex("mjd"));

    const std::pair<double, double> mjd_limits = GetLimits("mjd");
    min_mjd_ = mjd_limits.first;
    max_mjd_ = mjd_limits.second;
    delta_mjd_ = values.size() / (max_mjd_ - min_mjd_);
  };

  const double GetValue(const std::string& value, double mjd) {
    const int value_idx = GetColumnIndex(value);
    const int time_idx = GetColumnIndex("mjd");
    const std::pair<double, double> limits = GetLimits(value);
    const std::pair<int, int> index_limits = GetIndexLimits(value);

    if (limits.second != 0 && mjd > limits.first && mjd < limits.second) {
      int row = int((mjd - min_mjd_) * delta_mjd_);
      const double y0 = values[row][value_idx];
      const double x0 = row + min_mjd_;
      const double dy = values[row][value_idx] - values[row - 1][value_idx];
      return dy * (mjd - x0) + y0;
    }

    return std::numeric_limits<double>::quiet_NaN();
  }

 private:
  double min_mjd_;
  double max_mjd_;
  double delta_mjd_;
};

class ERFAFactory {
 protected:
  ERFAFactory() {}

  static std::shared_ptr<IERSTable> singleton_;

 public:
  ERFAFactory(ERFAFactory& other) = delete;
  void operator=(const ERFAFactory&) = delete;

  static std::shared_ptr<IERSTable> GetInstance(
      const std::string& value = IERS_PATH);
};

std::shared_ptr<IERSTable> ERFAFactory::singleton_ =
    std::shared_ptr<IERSTable>(nullptr);

std::shared_ptr<IERSTable> ERFAFactory::GetInstance(const std::string& path) {
  if (singleton_ == nullptr) {
    singleton_ = std::make_shared<IERSTable>(path);
  }
  return singleton_;
}

}  // namespace erfacpp
#endif  // ERFACPP_IERS